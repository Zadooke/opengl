package mage;

public class Block {
    public static Block Air = new Block(true);
    public static Block Normal = new Block(false);

    private boolean isTransparent;

    public Block(boolean isTransparent) { this.isTransparent = isTransparent; }
    public Block() { }

    public MeshData GenerateBlock(Block[][][] blocks, int x, int y, int z)
    {
        return MeshHelper.GenerateCube(this, blocks, x, y, z);
    }

    //Getters and Setters
    public boolean IsTransparent() { return isTransparent; }
}
