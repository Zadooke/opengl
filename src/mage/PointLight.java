package mage;

import org.joml.Vector3f;

public class PointLight {
    private float falloff;
    private Vector3f position;
    private Vector3f ambient;
    private Vector3f diffuse;
    private Vector3f specular;

    public PointLight(Vector3f position, Vector3f ambient, Vector3f diffuse, Vector3f specular, float falloff) {
        super();
        this.falloff = falloff;
        this.position = position;
        this.ambient = ambient;
        this.diffuse = diffuse;
        this.specular = specular;
    }

    public float getFalloff(){return falloff;}

    public Vector3f getPosition(){return position;}

    public Vector3f getAmbient() {
        return ambient;
    }

    public Vector3f getDiffuse() {
        return diffuse;
    }

    public Vector3f getSpecular() {
        return specular;
    }
    
    public void apply(Shader shader) {
        shader.setUniform("uLightPosition", position);
        shader.setUniform("uAmbientLight", ambient);
        shader.setUniform("uDiffuseLight", diffuse);
        shader.setUniform("uSpecularLight", specular);
    }
}
