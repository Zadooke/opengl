package mage;

import cg.MeshFactory;
import org.joml.Vector3f;

public class Chunk {
    public static ChunkSettings SETTINGS;
    public static int WIDTH = -1;
    public static int HEIGHT = -1;
    public static int WATER_HEIGHT = -1;
    public static float CUTOUT = -1;
    public static int SEED = -1;
    public static float FREQUENCY = -1;
    public static int OCTAVES = -1;
    public static float LACUNARITY = -1;
    public static float PERSISTANCE = -1;

    private Block[][][] blocks;
    private MeshData meshData;
    private Mesh mesh = null;
    public static float[][][] noise;

    private static final String shader = "phong_tex";

    public Chunk(Mesh outMesh) {
        mesh = outMesh;
    }

    public Mesh CreateChunk() {
        noise = Noise.PerlinNoise(WIDTH, HEIGHT, WIDTH, SEED, FREQUENCY, OCTAVES, LACUNARITY, PERSISTANCE);
        //blocks = DebugNoise();
        Noise();
        CreateMesh();
        mesh = MeshFactory.createChunk(meshData, shader);
        return mesh;
    }

    private void Noise() {
        blocks = new Block[WIDTH][HEIGHT][WIDTH];
        for (int z = 0; z < WIDTH; z++)
            for (int y = 0; y < HEIGHT; y++)
                for (int x = 0; x < WIDTH; x++) {
                    float intensity = noise[x][y][z];
                    if (intensity > CUTOUT || y < WATER_HEIGHT) blocks[x][y][z] = Block.Air;
                    else blocks[x][y][z] = Block.Normal;
                }
    }

    public static Block[][][] WaterChunk() {
        Block[][][] blocks = new Block[WIDTH][HEIGHT][WIDTH];
        for (int z = 0; z < WIDTH; z++)
            for (int y = 0; y < HEIGHT; y++)
                for (int x = 0; x < WIDTH; x++) {
                    if (y < WATER_HEIGHT)
                        blocks[x][y][z] = Block.Normal;
                    else
                        blocks[x][y][z] = Block.Air;
                }
        return blocks;
    }

    public static Mesh CreateWaterMesh(Block[][][] blocks, String shader) {
        MeshData meshData = new MeshData();
        for (int z = 0; z < WIDTH; z++)
            for (int y = 0; y < HEIGHT; y++)
                for (int x = 0; x < WIDTH; x++)
                    meshData.append(blocks[x][y][z].GenerateBlock(blocks, x, y, z));

        if (WIDTH % 2 == 0) meshData.addOffset(new Vector3f((-Chunk.WIDTH / 2), 0, (-Chunk.WIDTH / 2)));
        else meshData.addOffset(new Vector3f((-Chunk.WIDTH / 2) - 0.5f, 0, (-Chunk.WIDTH / 2) - 0.5f));
        return MeshFactory.createChunk(meshData, shader);
    }

    private void CreateMesh() {
        meshData = new MeshData();
        for (int z = 0; z < WIDTH; z++)
            for (int y = 0; y < HEIGHT; y++)
                for (int x = 0; x < WIDTH; x++)
                    meshData.append(blocks[x][y][z].GenerateBlock(blocks, x, y, z));

        if (WIDTH % 2 == 0) meshData.addOffset(new Vector3f((-Chunk.WIDTH / 2), 0, (-Chunk.WIDTH / 2)));
        else meshData.addOffset(new Vector3f((-Chunk.WIDTH / 2) - 0.5f, 0, (-Chunk.WIDTH / 2) - 0.5f));
    }

    private void CreateWater() {
        for (int z = 0; z < WIDTH; z++)
            for (int y = 0; y < WATER_HEIGHT; y++)
                for (int x = 0; x < WIDTH; x++)
                    blocks[x][y][z] = Block.Normal;
    }

    private Block[][][] DebugNoise() {
        Block[][][] blocks = new Block[Chunk.WIDTH][Chunk.HEIGHT][Chunk.WIDTH];

        for (int x = 0; x < Chunk.WIDTH; x++)
            for (int z = 0; z < Chunk.WIDTH; z++)
                for (int y = 0; y < Chunk.HEIGHT; y++)
                    blocks[x][y][z] = Block.Air;

        for (int x = 0; x < Chunk.WIDTH; x++)
            for (int z = 0; z < Chunk.WIDTH; z++)
                blocks[x][0][z] = Block.Normal;

        blocks[1][1][1] = Block.Normal;

        return blocks;
    }
}