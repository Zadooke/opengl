package mage;

/**
 * Created by Zadooke on 11/06/2017.
 */
public class ChunkSettings {
    public int width;
    public int height;
    public int waterHeight;
    public int seed;
    public int octaves;
    public float frequency;
    public float cutout;
    public float lacunarity;
    public float persistence;

    public ChunkSettings(int width, int height, int waterHeight, int seed, int octaves, float frequency, float cutout, float lacunarity, float persistence) {
        this.width = width;
        this.height = width;
        this.waterHeight = width;
        this.seed = width;
        this.octaves = width;
        this.frequency = width;
        this.cutout = width;
        this.lacunarity = width;
        this.persistence = width;
    }
}
