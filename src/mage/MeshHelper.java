package mage;

import org.joml.Vector2f;
import org.joml.Vector3f;

import java.util.ArrayList;
import java.util.Arrays;

public class MeshHelper {

    private static final float black = 0.6f;
    private static final int ATLAS_SIZE = 16;

    private static final Vector3f shadowColor = new Vector3f(black, black, black);

    public static MeshData createQuad(float size, float x, float y, float z, Vector3f color) {
        MeshData meshData = new MeshData();

        ArrayList<Vector3f> vertices;
        ArrayList<Integer> triangles;
        ArrayList<Vector2f> uv;
        ArrayList<Vector3f> colors;

        float halfSize = size / 2f;

        vertices = new ArrayList<>(Arrays.asList(new Vector3f[]{
                new Vector3f(-halfSize, 0, -halfSize),
                new Vector3f(halfSize, 0, -halfSize),
                new Vector3f(halfSize, 0, halfSize),
                new Vector3f(-halfSize, 0, halfSize)
        }));

        triangles = new ArrayList<>(Arrays.asList(new Integer[]{
                2, 1, 0,
                3, 2, 0
        }));

        uv = new ArrayList<>(Arrays.asList(new Vector2f[]{
                new Vector2f(0, 0),
                new Vector2f(1, 0),
                new Vector2f(1, 1),
                new Vector2f(0, 1)
        }));

        colors = new ArrayList<>(Arrays.asList(new Vector3f[]{
                color,
                color,
                color,
                color
        }));

        meshData.append(vertices, triangles, uv, colors);
        meshData.addOffset(new Vector3f(x, y, z));
        return meshData;
    }

    public static MeshData createQuadHeight(float[][] offset, int x, int z) {
        MeshData meshData = new MeshData();

        ArrayList<Vector3f> vertices;
        ArrayList<Integer> triangles;
        ArrayList<Vector2f> uv;
        ArrayList<Vector3f> colors;

        float halfSize = 0.5f;

        vertices = new ArrayList<>(Arrays.asList(new Vector3f[]{
                new Vector3f(-halfSize, offset[x][z], -halfSize),
                new Vector3f(halfSize, offset[x+1][z], -halfSize),
                new Vector3f(halfSize, offset[x+1][z+1], halfSize),
                new Vector3f(-halfSize, offset[x][z+1], halfSize)
        }));

        triangles = new ArrayList<>(Arrays.asList(new Integer[]{
                2, 1, 0,
                3, 2, 0
        }));

        uv = new ArrayList<>(Arrays.asList(new Vector2f[]{
                new Vector2f(0, 0),
                new Vector2f(1, 0),
                new Vector2f(1, 1),
                new Vector2f(0, 1)
        }));

        float value = offset[x][z];

        Vector3f color = new Vector3f(value, value, value);

        colors = new ArrayList<>(Arrays.asList(new Vector3f[]{
                color,
                color,
                color,
                color
        }));

        meshData.append(vertices, triangles, uv, colors);
        meshData.addOffset(new Vector3f(x, 0, z));
        return meshData;
    }

    public static MeshData createPentagon(Vector3f[] color) {
        MeshData meshData = new MeshData();

        ArrayList<Vector3f> vertices;
        ArrayList<Integer> triangles;
        ArrayList<Vector2f> uv;
        ArrayList<Vector3f> colors;

        vertices = new ArrayList<>(Arrays.asList(new Vector3f[]{
                new Vector3f(-.5f, 0, 0),   //0
                new Vector3f(.5f, 0, 0),    //1
                new Vector3f(.7f, .65f, 0),  //2
                new Vector3f(0, 1, 0),      //3
                new Vector3f(-.7f, .65f, 0), //4
                new Vector3f(0f, .4f, 0),   //5 center
        }));

        triangles = new ArrayList<>(Arrays.asList(new Integer[]{
                0, 1, 5,
                1, 2, 5,
                2, 3, 5,
                3, 4, 5,
                4, 0, 5
        }));

        uv = new ArrayList<>(Arrays.asList(new Vector2f[]{
        }));

        colors = new ArrayList<>(Arrays.asList(new Vector3f[]{
                color[0],
                color[1],
                color[2],
                color[3],
                color[4],
                color[5]
        }));

        meshData.append(vertices, triangles, uv, colors);
        meshData.addOffset(new Vector3f(0, -.5f, 0));
        return meshData;
    }

    public static boolean BlockExist(Block[][][] blocks, int x, int y, int z) {
        return x >= 0 && x < Chunk.WIDTH && y >= 0 && y < Chunk.HEIGHT && z >= 0 && z < Chunk.WIDTH && !blocks[x][y][z].IsTransparent();
    }

    public static MeshData GenerateCube(Block block, Block[][][] blocks, int x, int y, int z) {
        MeshData meshData = new MeshData();
        if (block == Block.Air)//If is a null block
            return meshData;

        //Atlas UVS
        ArrayList<Vector2f> dirt = AtlasUV(2, 0, ATLAS_SIZE);
        ArrayList<Vector2f> grass_side = AtlasUV(3, 0, ATLAS_SIZE);
        ArrayList<Vector2f> grass_top = AtlasUV(0, 0, ATLAS_SIZE);
        ArrayList<Vector2f> rock = AtlasUV(1, 0, ATLAS_SIZE);
        ArrayList<Vector2f> water = AtlasUV(0, 12, ATLAS_SIZE);

        ArrayList<Vector3f> vertices;
        ArrayList<Integer> triangles;
        ArrayList<Vector2f> uv;
        ArrayList<Vector3f> colors;

        //-Z TRAS
        if (z == 0 || blocks[x][y][z - 1].IsTransparent()) {

            vertices = new ArrayList<>(Arrays.asList(new Vector3f[]{
                    new Vector3f(0, 0, 0),  //0 Esq Baixo
                    new Vector3f(1, 0, 0),  //1 Dir Baixo
                    new Vector3f(1, 1, 0),  //2 Dir Cima
                    new Vector3f(0, 1, 0)   //3 Esq Cima
            }));

            triangles = new ArrayList<>(Arrays.asList(new Integer[]{
                    2, 1, 0,
                    3, 2, 0
            }));

            if (y > Chunk.HEIGHT - Chunk.noise[x][y][z] * Chunk.HEIGHT * 1.2f) {
                if (BlockExist(blocks, x, y + 1, z))
                    uv = dirt;
                else
                    uv = grass_side;
            } else
                uv = rock;

            if (y < Chunk.WATER_HEIGHT)
                uv = water;

            colors = new ArrayList<>(Arrays.asList(new Vector3f[]{
                    new Vector3f(1, 1, 1),
                    new Vector3f(1, 1, 1),
                    new Vector3f(1, 1, 1),
                    new Vector3f(1, 1, 1)
            }));

            // Linha Baixo
            if (BlockExist(blocks, x, y - 1, z - 1)) {
                colors.get(0).set(shadowColor);
                colors.get(1).set(shadowColor);
            }
            // Linha Cima
            if (BlockExist(blocks, x, y + 1, z - 1)) {
                colors.get(2).set(shadowColor);
                colors.get(3).set(shadowColor);
            }
            // Linha Direita
            if (BlockExist(blocks, x + 1, y, z - 1)) {
                colors.get(1).set(shadowColor);
                colors.get(2).set(shadowColor);
            }
            //Linha Esquerda
            if (BlockExist(blocks, x - 1, y, z - 1)) {
                colors.get(0).set(shadowColor);
                colors.get(3).set(shadowColor);
            }

            // Vertex Esq Baixo
            if (BlockExist(blocks, x - 1, y - 1, z - 1)) colors.get(0).set(shadowColor);
            // Vertex Dir Baixo
            if (BlockExist(blocks, x + 1, y - 1, z - 1)) colors.get(1).set(shadowColor);
            // Vertex Dir Cima
            if (BlockExist(blocks, x + 1, y + 1, z - 1)) colors.get(2).set(shadowColor);
            // Vertex Esq Cima
            if (BlockExist(blocks, x - 1, y + 1, z - 1)) colors.get(3).set(shadowColor);


            meshData.append(vertices, triangles, uv, colors);
        }

        //+Z FRENTE
        if (z == Chunk.WIDTH - 1 || blocks[x][y][z + 1].IsTransparent()) {

            vertices = new ArrayList<>(Arrays.asList(new Vector3f[]{
                    new Vector3f(0, 0, 1),  //0 Esq Baixo
                    new Vector3f(1, 0, 1),  //1 Dir Baixo
                    new Vector3f(1, 1, 1),  //2 Dir Cima
                    new Vector3f(0, 1, 1)   //3 Esq Cima
            }));

            triangles = new ArrayList<>(Arrays.asList(new Integer[]{
                    0, 1, 2,
                    0, 2, 3
            }));

            if (y > Chunk.HEIGHT - Chunk.noise[x][y][z] * Chunk.HEIGHT * 1.2f) {
                if (BlockExist(blocks, x, y + 1, z))
                    uv = dirt;
                else
                    uv = grass_side;
            } else
                uv = rock;
            if (y < Chunk.WATER_HEIGHT)
                uv = water;


            colors = new ArrayList<>(Arrays.asList(new Vector3f[]{
                    new Vector3f(1, 1, 1),
                    new Vector3f(1, 1, 1),
                    new Vector3f(1, 1, 1),
                    new Vector3f(1, 1, 1)
            }));


            // Linha Baixo
            if (BlockExist(blocks, x, y - 1, z + 1)) {
                colors.get(0).set(shadowColor);
                colors.get(1).set(shadowColor);
            }
            // Linha Cima
            if (BlockExist(blocks, x, y + 1, z + 1)) {
                colors.get(2).set(shadowColor);
                colors.get(3).set(shadowColor);
            }
            // Linha Direita
            if (BlockExist(blocks, x + 1, y, z + 1)) {
                colors.get(1).set(shadowColor);
                colors.get(2).set(shadowColor);
            }
            //Linha Esquerda
            if (BlockExist(blocks, x - 1, y, z + 1)) {
                colors.get(0).set(shadowColor);
                colors.get(3).set(shadowColor);
            }

            // Vertex Esq Baixo
            if (BlockExist(blocks, x - 1, y - 1, z + 1)) colors.get(0).set(shadowColor);
            // Vertex Dir Baixo
            if (BlockExist(blocks, x + 1, y - 1, z + 1)) colors.get(1).set(shadowColor);
            // Vertex Dir Cima
            if (BlockExist(blocks, x + 1, y + 1, z + 1)) colors.get(2).set(shadowColor);
            // Vertex Esq Cima
            if (BlockExist(blocks, x - 1, y + 1, z + 1)) colors.get(3).set(shadowColor);

            meshData.append(vertices, triangles, uv, colors);
        }
        //+X ?? Invertido
        if (x == Chunk.WIDTH - 1 || blocks[x + 1][y][z].IsTransparent()) {
            vertices = new ArrayList<>(Arrays.asList(new Vector3f[]{
                    new Vector3f(1, 0, 0), //0 Perto Baixo
                    new Vector3f(1, 0, 1), //1 Longe Baixo
                    new Vector3f(1, 1, 1), //2 Longe Cima
                    new Vector3f(1, 1, 0)  //3 Perto Cima
            }));

            triangles = new ArrayList<>(Arrays.asList(new Integer[]{
                    2, 1, 0,
                    3, 2, 0
            }));

            if (y > Chunk.HEIGHT - Chunk.noise[x][y][z] * Chunk.HEIGHT * 1.2f) {
                if (BlockExist(blocks, x, y + 1, z))
                    uv = dirt;
                else
                    uv = grass_side;
            } else
                uv = rock;
            if (y < Chunk.WATER_HEIGHT)
                uv = water;


            colors = new ArrayList<>(Arrays.asList(new Vector3f[]{
                    new Vector3f(1, 1, 1),
                    new Vector3f(1, 1, 1),
                    new Vector3f(1, 1, 1),
                    new Vector3f(1, 1, 1)
            }));

            //Linha Baixo
            if (BlockExist(blocks, x + 1, y - 1, z)) {
                colors.get(0).set(shadowColor);
                colors.get(1).set(shadowColor);
            }
            //Linha Cima
            if (BlockExist(blocks, x + 1, y + 1, z)) {
                colors.get(2).set(shadowColor);
                colors.get(3).set(shadowColor);
            }
            //Linha Longe
            if (BlockExist(blocks, x + 1, y, z + 1)) {
                colors.get(1).set(shadowColor);
                colors.get(2).set(shadowColor);
            }
            //Linha Perto
            if (BlockExist(blocks, x + 1, y, z - 1)) {
                colors.get(0).set(shadowColor);
                colors.get(3).set(shadowColor);
            }

            //Vertex Perto Baixo
            if (BlockExist(blocks, x + 1, y - 1, z - 1)) colors.get(0).set(shadowColor);
            //Vertex Longe Baixo
            if (BlockExist(blocks, x + 1, y - 1, z + 1)) colors.get(1).set(shadowColor);
            //Vertex Longe Cima
            if (BlockExist(blocks, x + 1, y + 1, z + 1)) colors.get(2).set(shadowColor);
            //Vertex Perto Cima
            if (BlockExist(blocks, x + 1, y + 1, z - 1)) colors.get(3).set(shadowColor);

            meshData.append(vertices, triangles, uv, colors);
        }

        //-X Invertido??
        if (x == 0 || blocks[x - 1][y][z].IsTransparent()) {

            vertices = new ArrayList<>(Arrays.asList(new Vector3f[]{
                    new Vector3f(0, 0, 0), //0 Perto Baixo
                    new Vector3f(0, 0, 1), //1 Longe Baixo
                    new Vector3f(0, 1, 1), //2 Longe Cima
                    new Vector3f(0, 1, 0)  //3 Perto Cima
            }));

            triangles = new ArrayList<>(Arrays.asList(new Integer[]{
                    0, 1, 2,
                    0, 2, 3
            }));

            if (y > Chunk.HEIGHT - Chunk.noise[x][y][z] * Chunk.HEIGHT * 1.2f) {
                if (BlockExist(blocks, x, y + 1, z))
                    uv = dirt;
                else
                    uv = grass_side;
            } else
                uv = rock;
            if (y < Chunk.WATER_HEIGHT)
                uv = water;


            colors = new ArrayList<>(Arrays.asList(new Vector3f[]{
                    new Vector3f(1, 1, 1),
                    new Vector3f(1, 1, 1),
                    new Vector3f(1, 1, 1),
                    new Vector3f(1, 1, 1)
            }));

            //Linha Baixo
            if (BlockExist(blocks, x - 1, y - 1, z)) {
                colors.get(0).set(shadowColor);
                colors.get(1).set(shadowColor);
            }
            //Linha Cima
            if (BlockExist(blocks, x - 1, y + 1, z)) {
                colors.get(2).set(shadowColor);
                colors.get(3).set(shadowColor);
            }
            //Linha Longe
            if (BlockExist(blocks, x - 1, y, z + 1)) {
                colors.get(1).set(shadowColor);
                colors.get(2).set(shadowColor);
            }
            //Linha Perto
            if (BlockExist(blocks, x - 1, y, z - 1)) {
                colors.get(0).set(shadowColor);
                colors.get(3).set(shadowColor);
            }

            //Vertex Perto Baixo
            if (BlockExist(blocks, x - 1, y - 1, z - 1)) colors.get(0).set(shadowColor);
            //Vertex Longe Baixo
            if (BlockExist(blocks, x - 1, y - 1, z + 1)) colors.get(1).set(shadowColor);
            //Vertex Longe Cima
            if (BlockExist(blocks, x - 1, y + 1, z + 1)) colors.get(2).set(shadowColor);
            //Vertex Perto Cima
            if (BlockExist(blocks, x - 1, y + 1, z - 1)) colors.get(3).set(shadowColor);

            meshData.append(vertices, triangles, uv, colors);
        }

        //+Y CIMA
        if (y == Chunk.HEIGHT - 1 || blocks[x][y + 1][z].IsTransparent()) {
            vertices = new ArrayList<>(Arrays.asList(new Vector3f[]{
                    new Vector3f(0, 1, 0), //0 Esq Perto
                    new Vector3f(1, 1, 0), //1 Dir Perto
                    new Vector3f(1, 1, 1), //2 Dir Longe
                    new Vector3f(0, 1, 1)  //3 Esq Longe
            }));

            triangles = new ArrayList<>(Arrays.asList(new Integer[]{
                    2, 1, 0,
                    3, 2, 0
            }));

            if (y > Chunk.HEIGHT - Chunk.noise[x][y][z] * Chunk.HEIGHT * 1.2f) {
                if (BlockExist(blocks, x, y + 1, z))
                    uv = dirt;
                else
                    uv = grass_top;
            } else
                uv = rock;
            if (y < Chunk.WATER_HEIGHT)
                uv = water;

            colors = new ArrayList<>(Arrays.asList(new Vector3f[]{
                    new Vector3f(1, 1, 1),
                    new Vector3f(1, 1, 1),
                    new Vector3f(1, 1, 1),
                    new Vector3f(1, 1, 1)
            }));


            //Linha Perto
            if (BlockExist(blocks, x, y + 1, z - 1)) {
                colors.get(0).set(shadowColor);
                colors.get(1).set(shadowColor);
            }
            //Linha Longe
            if (BlockExist(blocks, x, y + 1, z + 1)) {
                colors.get(2).set(shadowColor);
                colors.get(3).set(shadowColor);
            }
            //Linha Direita
            if (BlockExist(blocks, x + 1, y + 1, z)) {
                colors.get(1).set(shadowColor);
                colors.get(2).set(shadowColor);
            }
            //Linha Esquerda
            if (BlockExist(blocks, x - 1, y + 1, z)) {
                colors.get(0).set(shadowColor);
                colors.get(3).set(shadowColor);
            }

            //Vertex Perto Esq
            if (BlockExist(blocks, x - 1, y + 1, z - 1)) colors.get(0).set(shadowColor);
            //Vertex Perto Dir
            if (BlockExist(blocks, x + 1, y + 1, z - 1)) colors.get(1).set(shadowColor);
            //Vertex Longe Dir
            if (BlockExist(blocks, x + 1, y + 1, z + 1)) colors.get(2).set(shadowColor);
            //Vertex Longe Esq
            if (BlockExist(blocks, x - 1, y + 1, z + 1)) colors.get(3).set(shadowColor);

            meshData.append(vertices, triangles, uv, colors);
        }

        //-Y
        if (y == 0 || blocks[x][y - 1][z].IsTransparent()) {
            vertices = new ArrayList<>(Arrays.asList(new Vector3f[]{
                    new Vector3f(0, 0, 0),
                    new Vector3f(1, 0, 0),
                    new Vector3f(1, 0, 1),
                    new Vector3f(0, 0, 1)
            }));

            triangles = new ArrayList<>(Arrays.asList(new Integer[]{
                    2, 0, 1,
                    3, 0, 2
            }));

            uv = dirt;

            colors = new ArrayList<>(Arrays.asList(new Vector3f[]{
                    new Vector3f(1, 1, 1),
                    new Vector3f(1, 1, 1),
                    new Vector3f(1, 1, 1),
                    new Vector3f(1, 1, 1)
            }));

            colors.get(0).set(shadowColor);
            colors.get(1).set(shadowColor);
            colors.get(2).set(shadowColor);
            colors.get(3).set(shadowColor);

            meshData.append(vertices, triangles, uv, colors);
        }

        meshData.addOffset(new Vector3f(x, y, z));
        return meshData;
    }

    public static ArrayList<Vector2f> AtlasUV(int x, int y, int atlasSize) {
        float step = 1f / atlasSize;
        float minX = step * x;
        float maxX = step * (x + 1);
        float minY = step * y;
        float maxY = step * (y + 1);
        return new ArrayList<>(Arrays.asList(new Vector2f[]{
                new Vector2f(minX, maxY), //3
                new Vector2f(maxX, maxY), //2
                new Vector2f(maxX, minY), //1
                new Vector2f(minX, minY), //0
        }));
    }
}

