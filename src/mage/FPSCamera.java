package mage;

import org.joml.Matrix4f;
import org.joml.Quaternionf;
import org.joml.Vector3f;
import org.lwjgl.BufferUtils;

import java.nio.IntBuffer;

import static org.lwjgl.glfw.GLFW.*;

public class FPSCamera {

    private Vector3f position = new Vector3f(0, 0, -2);
    private Vector3f direction = new Vector3f(0, 0, 1);

    private Vector3f up = new Vector3f(0, 1, 0);
    private Vector3f target = new Vector3f(0, 0, 0);

    private float fov = (float) Math.toRadians(60);
    private float near = 0.1f;
    private float far = 1000.0f;

    private float angle = 0;

    public float speed = 10f;

    public Vector3f getPosition() {
        return position;
    }

    public void setPosition(Vector3f position) {
        this.position = position;
    }

    public Vector3f getDirection() {
        return direction;
    }

    public void setDirection(Vector3f direction) {
        this.direction = direction;
    }

    public Vector3f getUp() {
        return up;
    }

    public void setUp(Vector3f up) {
        this.up = up;
    }

    public Vector3f getTarget() {
        return target;
    }

    public void setTarget(Vector3f target) {
        this.target = target;
    }

    public float getFov() {
        return fov;
    }

    public void setFov(float fov) {
        this.fov = fov;
    }

    public float getNear() {
        return near;
    }

    public void setNear(float near) {
        this.near = near;
    }

    public float getFar() {
        return far;
    }

    public void setFar(float far) {
        this.far = far;
    }

    public float getAspect() {
        IntBuffer w = BufferUtils.createIntBuffer(1);
        IntBuffer h = BufferUtils.createIntBuffer(1);

        long window = glfwGetCurrentContext();
        glfwGetWindowSize(window, w, h);

        return w.get() / (float) h.get();
    }

    // Gerando as Matrizes que serão usadas pela câmera

    // Para aonde a câmera está olhando
    public Matrix4f getViewMatrix() {
        // Calcula o novo Target usando position e direciton
        position.add(direction.normalize(), target);

        return new Matrix4f().lookAt(position, target, up);
    }

    // Frustrum
    public Matrix4f getProjectionMatrix() {
        return new Matrix4f().perspective(fov, getAspect(), near, far);
    }

    // Movement

    public void move(float input, float secs) {
        Vector3f result = new Vector3f();

        input = input * speed * secs;
        direction.mul(input, result);

        position.add(result);
    }

    public void height(float input, float secs) {
        Vector3f result = new Vector3f();
        input = input * speed * secs;

        up.mul(input, result);
        position.add(result);
    }

    public void strafe(float input, float secs) {
        // Cria um vetor perpendicular ao plano de visão
        Vector3f perpendicular = new Vector3f();
        up.cross(direction, perpendicular).normalize();

        // Usa o eixo X do vetor de input como potência do movimento
        float dir = input * speed * secs;

        // Aplica a dir * o vetor perpendicular na posição
        position.add(perpendicular.mul(dir));
    }

    public void rotate(float angle) {
        Quaternionf q = new Quaternionf();
        q.rotateAxis(angle, up);

        direction.rotate(q);
    }

    public void rotateX(float angle) {
        // Analisa o angulo de entrada
        if (angle > 0) {
            if (this.angle + angle > 1.0f)
                return;


        } else if (angle < 0) {
            if (this.angle - angle < -1.0f)
                return;
        }

        this.angle += angle;

        Quaternionf q = new Quaternionf();

        // Cria um vetor perpendicular ao plano de visão
        Vector3f perpendicular = new Vector3f();
        up.cross(direction, perpendicular).normalize();

        q.rotateAxis(angle, perpendicular);

        direction.rotate(q);
    }
}
