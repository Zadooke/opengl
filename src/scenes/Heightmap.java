package scenes;

import cg.MeshFactory;
import mage.*;
import org.joml.Matrix4f;
import org.joml.Vector3f;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;

public class Heightmap implements Scene {
    private Keyboard keys = Keyboard.getInstance();

    private Mesh mesh;
    private FPSCamera camera = new FPSCamera();

    private float amplitude = 50;

    // Heightmap
    BufferedImage img;

    @Override
    public void init() {
        glPolygonMode(GL_FRONT_AND_BACK, GL_POLYGON);

        glEnable(GL_DEPTH_TEST);
        //glEnable(GL_CULL_FACE);

        camera.setPosition(new Vector3f(0, 5, -20));
        camera.speed = 50;

        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        // Carrega a imagem a ser usada como heightmap
        File file = Paths.get(".", "resources", "chavalier.jpg").normalize().toFile();

        try {
            img = ImageIO.read(file);

            mesh = MeshFactory.createQuadsWithImage(img, amplitude, "height");
            mesh.getShader().bind().setUniform("uAmplitude", amplitude).unbind();
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    private void HandleCameraInput(float secs) {
        if (keys.isDown(GLFW_KEY_A)) camera.strafe(1, secs);
        if (keys.isDown(GLFW_KEY_D)) camera.strafe(-1, secs);
        if (keys.isDown(GLFW_KEY_W)) camera.move(1, secs);
        if (keys.isDown(GLFW_KEY_S)) camera.move(-1, secs);
        if (keys.isDown(GLFW_KEY_E)) camera.height(1, secs);
        if (keys.isDown(GLFW_KEY_Q)) camera.height(-1, secs);
        if (keys.isDown(GLFW_KEY_UP)) camera.rotateX(-1 * secs);
        if (keys.isDown(GLFW_KEY_DOWN)) camera.rotateX(1 * secs);
        if (keys.isDown(GLFW_KEY_RIGHT)) camera.rotate(-1 * secs);
        if (keys.isDown(GLFW_KEY_LEFT)) camera.rotate(1 * secs);
    }

    @Override
    public void update(float secs) {
        if (keys.isPressed(GLFW_KEY_ESCAPE)) {
            glfwSetWindowShouldClose(glfwGetCurrentContext(), GLFW_TRUE);
            return;
        }

        HandleCameraInput(secs);

    }

    @Override
    public void draw() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        Shader shader = mesh.getShader();
        shader.bind()
                .setUniform("uProjection", camera.getProjectionMatrix())
                .setUniform("uView", camera.getViewMatrix());
        shader.unbind();

        mesh.setUniform("uWorld", new Matrix4f());
        mesh.draw();
    }

    @Override
    public void deinit() {
    }

    public static void main(String[] args) {
        new Window(new Heightmap(), "Height Map", 800, 600).show();
    }
}
