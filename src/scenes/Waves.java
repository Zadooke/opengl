package scenes;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;

import cg.Camera;
import cg.MeshFactory;
import mage.Keyboard;
import mage.Mesh;
import mage.Scene;
import mage.Window;
import org.joml.Matrix4f;
import org.joml.Vector3f;

public class Waves implements Scene {
    private Keyboard keys = Keyboard.getInstance();

    private Mesh mesh;
    private float angle;
    private Camera camera = new Camera();
    private float elapsedTime;

    @Override
    public void init() {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glEnable(GL_CULL_FACE);
        glEnable(GL_DEPTH_TEST);
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        mesh = MeshFactory.createQuads(.4f, 128, 128, new Vector3f(1, 1, 1), "wave");
        camera.setPosition(new Vector3f(0, 1.3f, 2));
    }

    @Override
    public void update(float secs) {
        elapsedTime += secs;

        if (keys.isPressed(GLFW_KEY_ESCAPE)) {
            glfwSetWindowShouldClose(glfwGetCurrentContext(), GL_TRUE);
            return;
        }

        if (keys.isDown(GLFW_KEY_A)) angle += Math.toRadians(180) * secs;
        if (keys.isDown(GLFW_KEY_D)) angle -= Math.toRadians(180) * secs;
        if (keys.isDown(GLFW_KEY_UP)) camera.setPosition(camera.getPosition().add(new Vector3f(0, 1 * secs, 0)));

        mesh.setUniform("uElapsedTime", elapsedTime);
    }

    @Override
    public void draw() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        mesh.getShader().bind()
                .setUniform("uProjection", camera.getProjectionMatrix())
                .setUniform("uView", camera.getViewMatrix())
                .unbind();
        mesh.setUniform("uWorld", new Matrix4f().rotateY(angle));
        mesh.draw();
    }

    @Override
    public void deinit() {
    }

    public static void main(String[] args) {
        new Window(new Waves(), "Waves", 1920, 1080).show();
    }
}
