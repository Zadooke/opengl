package scenes;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;

import cg.MeshFactory;
import mage.Chunk;
import mage.ChunkSettings;
import mage.FPSCamera;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import mage.*;
import org.joml.Matrix4f;
import org.joml.Vector3f;

import java.io.*;
import java.nio.file.Paths;

public class TerrainProcedural implements Scene {
    private static final String PATH = "C:/Imgs/";

    //Inputs
    private Keyboard keys = Keyboard.getInstance();

    //Meshes
    private Mesh chunkMesh;
    private Mesh waterMesh;

    //Skybox
    private Mesh skBox;

    //Materials
    private Material chunkMtl;
    private Material waterMtl;

    private Chunk chunk;
    private FPSCamera camera = new FPSCamera();

    private DirectionalLight light;

    private float elapsedTime = 0;

    private boolean wireFrame = false;

    @Override
    public void init() {
        glPolygonMode(GL_FRONT_AND_BACK, GL_POLYGON);

        // Carrega o JSon
        loadChunkSettings();

        camera.setPosition(new Vector3f(0, Chunk.HEIGHT, (-Chunk.WIDTH / 2) - 10));

        glEnable(GL_DEPTH_TEST);
        glEnable(GL_CULL_FACE);
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        chunk = new Chunk(chunkMesh);
        chunkMesh = chunk.CreateChunk();
        waterMesh = Chunk.CreateWaterMesh(Chunk.WaterChunk(), "phong_texAnim");
        skBox = MeshFactory.createSphere(200, 64, 64);

        light = new DirectionalLight(
                new Vector3f(1.0f, -1.0f, 1.0f),   //direction
                new Vector3f(0.8f, 0.8f, 0.8f),    //ambient
                new Vector3f(1.0f, 1.0f, 1.0f),    //diffuse
                new Vector3f(1.0f, 1.0f, 1.0f)     //specular
        );

        chunkMtl = new Material(
                new Vector3f(1.0f, 1.0f, 1.0f), //ambient
                new Vector3f(0.7f, 0.7f, 0.7f), //diffuse
                new Vector3f(0.5f, 0.5f, 0.5f), //specular
                2.0f);                          //Specular power

        waterMtl = new Material(
                new Vector3f(1.0f, 1.0f, 1.0f), //ambient
                new Vector3f(0.7f, 0.7f, 0.7f), //diffuse
                new Vector3f(0.5f, 0.5f, 0.5f), //specular
                2.0f);                          //Specular power

        File file = Paths.get(".", "Resources", "minecraft_atlas.png").normalize().toFile();

        chunkMtl.setTexture("uTexture", new Texture(file.getPath(), new TextureParameters(GL_NEAREST, GL_NEAREST, GL_CLAMP, GL_CLAMP)));
        waterMtl.setTexture("uTexture", new Texture(file.getPath(), new TextureParameters(GL_NEAREST, GL_NEAREST, GL_REPEAT, GL_REPEAT)));
    }

    private void HandleCameraInput(float secs) {
        if (keys.isDown(GLFW_KEY_A)) camera.strafe(1, secs);
        if (keys.isDown(GLFW_KEY_D)) camera.strafe(-1, secs);
        if (keys.isDown(GLFW_KEY_W)) camera.move(1, secs);
        if (keys.isDown(GLFW_KEY_S)) camera.move(-1, secs);
        if (keys.isDown(GLFW_KEY_E)) camera.height(1, secs);
        if (keys.isDown(GLFW_KEY_Q)) camera.height(-1, secs);
        if (keys.isDown(GLFW_KEY_UP)) camera.rotateX(-1 * secs);
        if (keys.isDown(GLFW_KEY_DOWN)) camera.rotateX(1 * secs);
        if (keys.isDown(GLFW_KEY_RIGHT)) camera.rotate(-1 * secs);
        if (keys.isDown(GLFW_KEY_LEFT)) camera.rotate(1 * secs);
    }

    private void ChangeGLMode(){
        wireFrame = !wireFrame;

        if (wireFrame)
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        else
            glPolygonMode(GL_FRONT_AND_BACK, GL_POLYGON);
    }

    @Override
    public void update(float secs) {
        if (keys.isPressed(GLFW_KEY_ESCAPE)) {
            glfwSetWindowShouldClose(glfwGetCurrentContext(), GL_TRUE);
            return;
        }
        HandleCameraInput(secs);

        if (keys.isPressed(GLFW_KEY_Z)) {
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        }

        if(keys.isPressed(GLFW_KEY_X)){
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }

        elapsedTime += secs / 10;
        Shader waterShader = waterMesh.getShader();
        waterShader.bind()
                .setUniform("uTime", elapsedTime);
        waterShader.unbind();
    }

    @Override
    public void draw() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        Shader chunkShader = chunkMesh.getShader();
        chunkShader.bind()
                .setUniform("uProjection", camera.getProjectionMatrix())
                .setUniform("uView", camera.getViewMatrix())
                .setUniform("uCameraPosition", camera.getPosition());
        light.apply(chunkShader);
        chunkMtl.apply(chunkShader);
        chunkShader.unbind();

        Shader waterShader = waterMesh.getShader();
        waterShader.bind()
                .setUniform("uProjection", camera.getProjectionMatrix())
                .setUniform("uView", camera.getViewMatrix())
                .setUniform("uCameraPosition", camera.getPosition());
        light.apply(waterShader);
        waterMtl.apply(waterShader);
        waterShader.unbind();

        skBox.getShader().bind()
                .setUniform("uProjection", camera.getProjectionMatrix())
                .setUniform("uView", camera.getViewMatrix())
                .unbind();
        skBox.setUniform("uWorld", new Matrix4f());
        skBox.draw();

        chunkMesh.setUniform("uWorld", new Matrix4f());
        chunkMesh.draw();

        waterMesh.setUniform("uWorld", new Matrix4f());
        waterMesh.draw();
    }

    @Override
    public void deinit() {
    }

    private void loadChunkSettings() {
        try (Reader reader = new InputStreamReader(Chunk.class.getResourceAsStream("/ChunkSettings.json"), "UTF-8")) {
            Gson gson = new GsonBuilder().create();
            Chunk.SETTINGS = gson.fromJson(reader, ChunkSettings.class);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Chunk.WIDTH = Chunk.SETTINGS.width;
        Chunk.HEIGHT = Chunk.SETTINGS.height;
        Chunk.WATER_HEIGHT = Chunk.SETTINGS.waterHeight;
        Chunk.CUTOUT = Chunk.SETTINGS.cutout;
        Chunk.SEED = Chunk.SETTINGS.seed;
        Chunk.FREQUENCY = Chunk.SETTINGS.frequency;
        Chunk.OCTAVES = Chunk.SETTINGS.octaves;
        Chunk.LACUNARITY = Chunk.SETTINGS.lacunarity;
        Chunk.PERSISTANCE = Chunk.SETTINGS.persistence;
    }

    public static void main(String[] args) {
        Window window = new Window(new TerrainProcedural(), "TerrainProcedural", 1920, 1080);
        window.show();
    }
}
