package scenes;

import cg.MeshFactory;
import mage.*;
import org.joml.Matrix4f;
import org.joml.Vector3f;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;


public class Sphere implements Scene {
    private Keyboard keys = Keyboard.getInstance();

    private Mesh mesh;
    private FPSCamera camera = new FPSCamera();

    private PointLight light;
    private Material mat;

    private float elapsedTime;

    @Override
    public void init() {
        glEnable(GL_DEPTH_TEST);
        //glEnable(GL_CULL_FACE);

        glPolygonMode(GL_FRONT_AND_BACK, GL_POLYGON);

        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        light = new PointLight(
                new Vector3f(0, 0, 0),   //position
                new Vector3f(0.8f, 0.8f, 0.8f),    //ambient
                new Vector3f(1.0f, 1.0f, 1.0f),    //diffuse
                new Vector3f(1.0f, 1.0f, 1.0f),     //specular
                0.1f                                //fallof
        );

        mat = new Material(
                new Vector3f(1.0f, 1.0f, 1.0f), //ambient
                new Vector3f(0.7f, 0.7f, 0.7f), //diffuse
                new Vector3f(0.5f, 0.5f, 0.5f), //specular
                0);                          //Specular power)

        mesh = MeshFactory.createSphere(1, 64, 64);

        camera.setPosition(new Vector3f(0,0,-2));
        camera.speed = 1;
    }

    private void HandleCameraInput(float secs) {
        if (keys.isDown(GLFW_KEY_A)) camera.strafe(1, secs);
        if (keys.isDown(GLFW_KEY_D)) camera.strafe(-1, secs);
        if (keys.isDown(GLFW_KEY_W)) camera.move(1, secs);
        if (keys.isDown(GLFW_KEY_S)) camera.move(-1, secs);
        if (keys.isDown(GLFW_KEY_E)) camera.height(1, secs);
        if (keys.isDown(GLFW_KEY_Q)) camera.height(-1, secs);
        if (keys.isDown(GLFW_KEY_UP)) camera.rotateX(-1 * secs);
        if (keys.isDown(GLFW_KEY_DOWN)) camera.rotateX(1 * secs);
        if (keys.isDown(GLFW_KEY_RIGHT)) camera.rotate(-1 * secs);
        if (keys.isDown(GLFW_KEY_LEFT)) camera.rotate(1 * secs);
    }

    @Override
    public void update(float secs) {
        if (keys.isPressed(GLFW_KEY_ESCAPE)) {
            glfwSetWindowShouldClose(glfwGetCurrentContext(), GL_TRUE);
            return;
        }

        HandleCameraInput(secs);

        elapsedTime += secs * 4;

        light.getPosition().x = (float)Math.sin(elapsedTime) * 2;
        light.getPosition().z = (float)Math.cos(elapsedTime) * 2;


    }

    @Override
    public void draw() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        mesh.getShader().bind()
                .setUniform("uProjection", camera.getProjectionMatrix())
                .setUniform("uView", camera.getViewMatrix())
                .unbind();
        mesh.setUniform("uWorld", new Matrix4f());
        mesh.draw();
    }

    @Override
    public void deinit() {
    }

    public static void main(String[] args) {
        new Window(new Sphere(), "Sphere", 1920, 1080).show();
    }
}
