package scenes;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;

import cg.Camera;
import cg.MeshFactory;
import mage.Keyboard;
import mage.Mesh;
import mage.Scene;
import mage.Window;
import org.joml.Matrix4f;
import org.joml.Vector3f;


public class Pentagon implements Scene {
    private Keyboard keys = Keyboard.getInstance();

    private Mesh mesh;
    private Camera camera = new Camera();

    @Override
    public void init() {
        camera.setPosition(new Vector3f(0, 0, 2));
        camera.setTarget(new Vector3f(0, 0, -1));
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_CULL_FACE);
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        mesh = MeshFactory.createPentagon(new Vector3f[]{
                new Vector3f(1, 0, 0),
                new Vector3f(0, 1, 0),
                new Vector3f(0, 0, 1),
                new Vector3f(1, 1, 0),
                new Vector3f(1, 0, 1),
                new Vector3f(0, 1, 1)
        },"basic");
    }

    @Override
    public void update(float secs) {
        if (keys.isPressed(GLFW_KEY_ESCAPE)) {
            glfwSetWindowShouldClose(glfwGetCurrentContext(), GL_TRUE);
            return;
        }
    }

    @Override
    public void draw() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        mesh.getShader().bind()
                .setUniform("uProjection", camera.getProjectionMatrix())
                .setUniform("uView", camera.getViewMatrix())
                .unbind();
        mesh.setUniform("uWorld", new Matrix4f().identity());
        mesh.draw();
    }

    @Override
    public void deinit() {
    }

    public static void main(String[] args) {
        new Window(new Pentagon(), "Pentagon", 800, 600).show();
    }
}
