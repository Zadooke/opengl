package scenes;

import cg.Camera;
import cg.MeshFactory;
import mage.Keyboard;
import mage.Mesh;
import mage.Scene;
import mage.Window;
import org.joml.Matrix4f;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;


public class Cube implements Scene {
    private Keyboard keys = Keyboard.getInstance();

    private Mesh mesh;
    private Camera camera = new Camera();
    private float cameraAngleY;
    private float cameraAngleX;

    @Override
    public void init() {
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_CULL_FACE);
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        mesh = MeshFactory.createCube();
    }

    @Override
    public void update(float secs) {
        if (keys.isPressed(GLFW_KEY_ESCAPE)) {
            glfwSetWindowShouldClose(glfwGetCurrentContext(), GL_TRUE);
            return;
        }

        if (keys.isDown(GLFW_KEY_A)) cameraAngleY -= Math.toRadians(180) * secs;
        if (keys.isDown(GLFW_KEY_D)) cameraAngleY += Math.toRadians(180) * secs;
        if (keys.isDown(GLFW_KEY_W)) cameraAngleX -= Math.toRadians(180) * secs;
        if (keys.isDown(GLFW_KEY_S)) cameraAngleX += Math.toRadians(180) * secs;
    }

    @Override
    public void draw() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        mesh.getShader().bind()
                .setUniform("uProjection", camera.getProjectionMatrix())
                .setUniform("uView", camera.getViewMatrix())
                .unbind();
        mesh.setUniform("uWorld", new Matrix4f().rotateY(cameraAngleY).rotateX(cameraAngleX));
        mesh.draw();
    }

    @Override
    public void deinit() {
    }

    public static void main(String[] args) {
        new Window(new Cube(), "Cube", 800, 600).show();
    }
}
