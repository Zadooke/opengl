package cg;

import mage.*;
import org.joml.Vector2f;
import org.joml.Vector3f;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.List;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;

public class MeshFactory {

    public static Mesh createCube() {
        return createCube(
                new Vector3f(0.988f, 0.663f, 0.522f),
                new Vector3f(0.522f, 0.792f, 0.365f),
                new Vector3f(0.459f, 0.537f, 0.749f),
                new Vector3f(0.976f, 0.549f, 0.714f),
                new Vector3f(0.647f, 0.537f, 0.757f),
                new Vector3f(0.753f, 0.729f, 0.600f)
        );
    }

    public static Mesh createCube(Vector3f color) {
        return createCube(color, color, color, color, color, color);
    }

    public static Mesh createCube(Vector3f frontColor, Vector3f backColor, Vector3f topColor, Vector3f bottomColor, Vector3f rightColor, Vector3f leftColor) {
        return new MeshBuilder()
                .addVector3fAttribute("aPosition",
                        //Face próxima
                        -0.5f, 0.5f, 0.5f,  //0
                        0.5f, 0.5f, 0.5f,  //1
                        -0.5f, -0.5f, 0.5f,  //2
                        0.5f, -0.5f, 0.5f,  //3
                        //Face afastada
                        -0.5f, 0.5f, -0.5f,  //4
                        0.5f, 0.5f, -0.5f,  //5
                        -0.5f, -0.5f, -0.5f,  //6
                        0.5f, -0.5f, -0.5f,  //7
                        //Face superior
                        -0.5f, 0.5f, 0.5f,  //8
                        0.5f, 0.5f, 0.5f,  //9
                        -0.5f, 0.5f, -0.5f,  //10
                        0.5f, 0.5f, -0.5f,  //11
                        //Face inferior
                        -0.5f, -0.5f, 0.5f,  //12
                        0.5f, -0.5f, 0.5f,  //13
                        -0.5f, -0.5f, -0.5f,  //14
                        0.5f, -0.5f, -0.5f,  //15
                        //Face direita
                        0.5f, -0.5f, 0.5f,  //16
                        0.5f, 0.5f, 0.5f,  //17
                        0.5f, -0.5f, -0.5f,  //18
                        0.5f, 0.5f, -0.5f,  //19
                        //Face esquerda
                        -0.5f, -0.5f, 0.5f,   //20
                        -0.5f, 0.5f, 0.5f,   //21
                        -0.5f, -0.5f, -0.5f,  //22
                        -0.5f, 0.5f, -0.5f)  //23
                .addVector3fAttribute("aColor",
                        //Face próxima
                        frontColor,
                        frontColor,
                        frontColor,
                        frontColor,
                        //Face afastada
                        backColor,
                        backColor,
                        backColor,
                        backColor,
                        //Face superior
                        topColor,
                        topColor,
                        topColor,
                        topColor,
                        //Face inferior
                        bottomColor,
                        bottomColor,
                        bottomColor,
                        bottomColor,
                        //Face direita
                        rightColor,
                        rightColor,
                        rightColor,
                        rightColor,
                        //Face esquerda
                        leftColor,
                        leftColor,
                        leftColor,
                        leftColor)
                .setIndexBuffer(
                        //Face próxima
                        0, 2, 3,
                        0, 3, 1,
                        //Face afastada
                        4, 7, 6,
                        4, 5, 7,
                        //Face superior
                        8, 11, 10,
                        8, 9, 11,
                        //Face inferior
                        12, 14, 15,
                        12, 15, 13,
                        //Face direita
                        16, 18, 19,
                        16, 19, 17,
                        //Face esquerda
                        20, 23, 22,
                        20, 21, 23)
                .loadShader("/shaders/basic")
                .create();
    }

    public static Mesh createCube(String shader) {
        return new MeshBuilder()
                .addVector3fAttribute("aPosition",
                        //Face próxima
                        -0.5f, 0.5f, 0.5f,  //0
                        0.5f, 0.5f, 0.5f,  //1
                        -0.5f, -0.5f, 0.5f,  //2
                        0.5f, -0.5f, 0.5f,  //3
                        //Face afastada
                        -0.5f, 0.5f, -0.5f,  //4
                        0.5f, 0.5f, -0.5f,  //5
                        -0.5f, -0.5f, -0.5f,  //6
                        0.5f, -0.5f, -0.5f,  //7
                        //Face superior
                        -0.5f, 0.5f, 0.5f,  //8
                        0.5f, 0.5f, 0.5f,  //9
                        -0.5f, 0.5f, -0.5f,  //10
                        0.5f, 0.5f, -0.5f,  //11
                        //Face inferior
                        -0.5f, -0.5f, 0.5f,  //12
                        0.5f, -0.5f, 0.5f,  //13
                        -0.5f, -0.5f, -0.5f,  //14
                        0.5f, -0.5f, -0.5f,  //15
                        //Face direita
                        0.5f, -0.5f, 0.5f,  //16
                        0.5f, 0.5f, 0.5f,  //17
                        0.5f, -0.5f, -0.5f,  //18
                        0.5f, 0.5f, -0.5f,  //19
                        //Face esquerda
                        -0.5f, -0.5f, 0.5f,   //20
                        -0.5f, 0.5f, 0.5f,   //21
                        -0.5f, -0.5f, -0.5f,  //22
                        -0.5f, 0.5f, -0.5f)  //23
                .addVector3fAttribute("aNormal",
                        //Face próxima
                        0, 0, 1,
                        0, 0, 1,
                        0, 0, 1,
                        0, 0, 1,
                        //Face afastada
                        0, 0, -1,
                        0, 0, -1,
                        0, 0, -1,
                        0, 0, -1,
                        //Face superior
                        0, 1, 0,
                        0, 1, 0,
                        0, 1, 0,
                        0, 1, 0,
                        //Face inferior
                        0, -1, 0,
                        0, -1, 0,
                        0, -1, 0,
                        0, -1, 0,
                        //Face direita
                        1, 0, 0,
                        1, 0, 0,
                        1, 0, 0,
                        1, 0, 0,
                        //Face esquerda
                        -1, 0, 0,
                        -1, 0, 0,
                        -1, 0, 0,
                        -1, 0, 0)
                .setIndexBuffer(
                        //Face próxima
                        0, 2, 3,
                        0, 3, 1,
                        //Face afastada
                        4, 7, 6,
                        4, 5, 7,
                        //Face superior
                        8, 11, 10,
                        8, 9, 11,
                        //Face inferior
                        12, 14, 15,
                        12, 15, 13,
                        //Face direita
                        16, 18, 19,
                        16, 19, 17,
                        //Face esquerda
                        20, 23, 22,
                        20, 21, 23)
                .loadShader("/shaders/" + shader)
                .create();
    }

    public static Mesh createQuads(float quadSize, int xCount, int zCount, Vector3f color, String shaderName) {
        MeshData meshData = new MeshData();
        for (int z = 0; z < zCount; z++)
            for (int x = 0; x < xCount; x++)
                meshData.append(MeshHelper.createQuad(quadSize, x * quadSize, 0, z * quadSize, color));

        meshData.addOffset(new Vector3f(-xCount / 2 * quadSize, 0, -zCount / 2 * quadSize));

        return new MeshBuilder()
                .addVector3fAttribute("aPosition", meshData.vertices)
                .addVector3fAttribute("aColor", meshData.colors)
                .setIndexBuffer(meshData.triangles)
                .loadShader("/shaders/" + shaderName)
                .create();

    }

    public static Mesh createQuadsWithImage(BufferedImage img, float amplitude, String shaderName) {
        MeshData meshData = new MeshData();

        // Tamanhos
        int zCount = img.getHeight() - 1;
        int xCount = img.getWidth() - 1;

        // Pega o offset da imagem
        float[][] offset = createOffsets(img, amplitude);

        for (int z = 0; z < zCount; z++)
            for (int x = 0; x < xCount; x++)
                meshData.append(MeshHelper.createQuadHeight(offset, x, z));
        //meshData.append(MeshHelper.createQuad(quadSize, x * quadSize, 0, z * quadSize, color));

        meshData.addOffset(new Vector3f(-xCount / 2, 0, -zCount / 2));

        return new MeshBuilder()
                .addVector3fAttribute("aPosition", meshData.vertices)
                .setIndexBuffer(meshData.triangles)
                .loadShader("/shaders/" + shaderName)
                .create();

    }

    public static Vector3f RGBToVec01(int color) {
        Color c = new Color(color);

        Vector3f col = new Vector3f(c.getRed(), c.getRed(), c.getRed());
        col.mul(1.0f / 255);

        return col;
    }

    private static float[][] createOffsets(BufferedImage img, float amplitude) {
        float[][] offsets = new float[img.getWidth()][img.getHeight()];

        for (int i = 0; i < img.getWidth(); i++)
            for (int j = 0; j < img.getHeight(); j++) {
                // Pega a cor da imagem no pixel atual
                float value = RGBToVec01(img.getRGB(i, j)).x * amplitude;
                offsets[i][j] = value;
            }

        return offsets;
    }

    public static Mesh createQuadsWithImage(float quadSize, BufferedImage img, float amplitude, String shaderName) {
        // Pega os tamanhos da imagem
        int xCount = img.getWidth();
        int zCount = img.getHeight();

        // Preenche um ArrayList com todos os offsets da imagem
        //float[] offsets = createOffsets(img, amplitude);
        int curOffset = 0;

        // Cria a mesh
        int totalIndex = 0;

        ArrayList<Vector3f> positions = new ArrayList<>();
        ArrayList<Vector3f> colors = new ArrayList<>();
        ArrayList<Integer> indexes = new ArrayList<>();

        for (int line = 0; line < zCount; line++) {
            for (int colum = 0; colum < xCount; colum++) {


                positions.add(new Vector3f(((colum * quadSize) - quadSize) - zCount * quadSize / 2, 0, ((line * quadSize) + quadSize) - xCount * quadSize / 2));
                //curOffset++;

                positions.add(new Vector3f(((colum * quadSize) + quadSize) - zCount * quadSize / 2, 0, ((line * quadSize) + quadSize) - xCount * quadSize / 2));
                positions.add(new Vector3f(((colum * quadSize) + quadSize) - zCount * quadSize / 2, 0, ((line * quadSize) - quadSize) - xCount * quadSize / 2));
                positions.add(new Vector3f(((colum * quadSize) - quadSize) - zCount * quadSize / 2, 0, ((line * quadSize) - quadSize) - xCount * quadSize / 2));

                //curOffset++;

                colors.add(new Vector3f(1, 1, 1));
                colors.add(new Vector3f(1, 1, 1));
                colors.add(new Vector3f(1, 1, 1));
                colors.add(new Vector3f(1, 1, 1));

                indexes.add(totalIndex);
                indexes.add(totalIndex + 1);
                indexes.add(totalIndex + 2);
                indexes.add(totalIndex + 2);
                indexes.add(totalIndex + 3);
                indexes.add(totalIndex);

                totalIndex += 4;
            }
        }


        return new MeshBuilder()
                .addVector3fAttribute("aPosition", positions)
                .addVector3fAttribute("aColor", colors)
                .setIndexBuffer(indexes)
                .loadShader("/shaders/" + shaderName)
                .create();

    }

    public static Mesh createPentagon(Vector3f[] colors, String shaderName) {
        MeshData meshData = MeshHelper.createPentagon(colors);

        return new MeshBuilder()
                .addVector3fAttribute("aPosition", meshData.vertices)
                .addVector3fAttribute("aColor", meshData.colors)
                .setIndexBuffer(meshData.triangles)
                .loadShader("/shaders/" + shaderName)
                .create();

    }

    public static Mesh createChunk(MeshData meshData, String shaderName) {
        return new MeshBuilder()
                .addVector3fAttribute("aPosition", meshData.vertices)
                .addVector3fAttribute("aColor", meshData.colors)
                .addVector2fAttribute("aTexCoord", meshData.uv)
                .setIndexBuffer(meshData.triangles)
                .loadShader("/shaders/" + shaderName)
                .create();
    }

    private static final float pi = 3.1415f;

    public static Mesh createSphere(float radius, float divX, float divY) {
        float longitude_step = 360.0f / divX; // Passo para circular no eixo X LONGITUDE
        float latitude_step = 360.0f / divY; // Passo para circular no eixo Y, em cada direção LATITUDE

        MeshData meshData = new MeshData();

        ArrayList<Vector3f> positions = new ArrayList<>();

        // Preenchendo a parte de cima
        for (float phi = 0; phi < 360; phi += latitude_step) {
            for (float th = 0; th < 180; th += longitude_step) {
                // Encontrando a posição do primeiro vertice

                float x1 = (float) Math.sin(Math.toRadians(th)) * (float) Math.cos(Math.toRadians(phi)) * radius;
                float z1 = (float) Math.sin(Math.toRadians(th)) * (float) Math.sin(Math.toRadians(phi)) * radius;
                float y1 = (float) Math.cos(Math.toRadians(th)) * radius;

                float x2 = (float) Math.sin(Math.toRadians(th)) * (float) Math.cos(Math.toRadians(phi + latitude_step)) * radius;
                float z2 = (float) Math.sin(Math.toRadians(th)) * (float) Math.sin(Math.toRadians(phi + latitude_step)) * radius;
                float y2 = (float) Math.cos(Math.toRadians(th)) * radius;

                float x3 = (float) Math.sin(Math.toRadians(th + longitude_step)) * (float) Math.cos(Math.toRadians(phi + latitude_step)) * radius;
                float z3 = (float) Math.sin(Math.toRadians(th + longitude_step)) * (float) Math.sin(Math.toRadians(phi + latitude_step)) * radius;
                float y3 = (float) Math.cos(Math.toRadians(th + longitude_step)) * radius;

                float x4 = (float) Math.sin(Math.toRadians(th + longitude_step)) * (float) Math.cos(Math.toRadians(phi)) * radius;
                float z4 = (float) Math.sin(Math.toRadians(th + longitude_step)) * (float) Math.sin(Math.toRadians(phi)) * radius;
                float y4 = (float) Math.cos(Math.toRadians(th + longitude_step)) * radius;

                ArrayList<Vector3f> pos = new ArrayList<>(Arrays.asList(new Vector3f[]{
                        new Vector3f(z1, y1, x1),
                        new Vector3f(z2, y2, x2),
                        new Vector3f(z3, y3, x3),
                        new Vector3f(z4, y4, x4)
                }));

                ArrayList<Integer> triangles = new ArrayList<>(Arrays.asList(new Integer[]{
                        2, 0, 1,
                        3, 0, 2
                }));

//                  CORES
                float color = th / 90;

                ArrayList<Vector3f> colors = new ArrayList<>(Arrays.asList(new Vector3f[]{
                        new Vector3f(0.01f,0.1f, 1 - color/5),
                        new Vector3f(0.01f,0.1f, 1 - color/5),
                        new Vector3f(0.01f,0.1f, 1 - color/5),
                        new Vector3f(0.01f,0.1f, 1 - color/5)
                }));

                Vector3f n1 = new Vector3f(-x1, -y1, -z1).normalize();

                Vector3f n2 = new Vector3f(-x2, -y2, -z2).normalize();
                Vector3f n3 = new Vector3f(-x3, -y3, -z3).normalize();
                Vector3f n4 = new Vector3f(-x4, -y4, -z4).normalize();

                ArrayList<Vector3f> normals = new ArrayList<>(Arrays.asList(new Vector3f[]{
                        n1,
                        n2,
                        n3,
                        n4
                }));

                ArrayList<Vector2f> uv = new ArrayList<>(Arrays.asList(new Vector2f[]{
                        new Vector2f((float)(Math.atan2(n1.x, n1.z) / 2*pi) + 0.5f, n1.y * 0.5f + 0.5f),
                        new Vector2f((float)(Math.atan2(n2.x, n1.z) / 2*pi) + 0.5f, n2.y * 0.5f + 0.5f),
                        new Vector2f((float)(Math.atan2(n3.x, n1.z) / 2*pi) + 0.5f, n3.y * 0.5f + 0.5f),
                        new Vector2f((float)(Math.atan2(n4.x, n1.z) / 2*pi) + 0.5f, n4.y * 0.5f + 0.5f)
                }));

//                ArrayList<Vector3f> colors = new ArrayList<>(Arrays.asList(new Vector3f[]{
//                        new Vector3f(1f, 1f, 1),
//                        new Vector3f(1f, 1f, 1),
//                        new Vector3f(1f, 1f, 1),
//                        new Vector3f(1f, 1f, 1)
//                }));

                meshData.append(pos, triangles, null, colors);

            }
        }

        return new MeshBuilder()
                .addVector3fAttribute("aPosition", meshData.vertices)
                .addVector3fAttribute("aColor", meshData.colors)
                .setIndexBuffer(meshData.triangles)
                .loadShader("/shaders/basic")
                .create();

    }

    public static Mesh createSkySphere(float radius, float divX, float divY) {
        float longitude_step = 360.0f / divX; // Passo para circular no eixo X LONGITUDE
        float latitude_step = 360.0f / divY; // Passo para circular no eixo Y, em cada direção LATITUDE

        MeshData meshData = new MeshData();

        ArrayList<Vector3f> normals = new ArrayList<>();

        // Preenchendo a parte de cima
        for (float phi = 0; phi < 360; phi += latitude_step) {
            for (float th = 0; th < 180; th += longitude_step) {
                // Encontrando a posição do primeiro vertice

                float x1 = (float) Math.sin(Math.toRadians(th)) * (float) Math.cos(Math.toRadians(phi)) * radius;
                float z1 = (float) Math.sin(Math.toRadians(th)) * (float) Math.sin(Math.toRadians(phi)) * radius;
                float y1 = (float) Math.cos(Math.toRadians(th)) * radius;

                float x2 = (float) Math.sin(Math.toRadians(th)) * (float) Math.cos(Math.toRadians(phi + latitude_step)) * radius;
                float z2 = (float) Math.sin(Math.toRadians(th)) * (float) Math.sin(Math.toRadians(phi + latitude_step)) * radius;
                float y2 = (float) Math.cos(Math.toRadians(th)) * radius;

                float x3 = (float) Math.sin(Math.toRadians(th + longitude_step)) * (float) Math.cos(Math.toRadians(phi + latitude_step)) * radius;
                float z3 = (float) Math.sin(Math.toRadians(th + longitude_step)) * (float) Math.sin(Math.toRadians(phi + latitude_step)) * radius;
                float y3 = (float) Math.cos(Math.toRadians(th + longitude_step)) * radius;

                float x4 = (float) Math.sin(Math.toRadians(th + longitude_step)) * (float) Math.cos(Math.toRadians(phi)) * radius;
                float z4 = (float) Math.sin(Math.toRadians(th + longitude_step)) * (float) Math.sin(Math.toRadians(phi)) * radius;
                float y4 = (float) Math.cos(Math.toRadians(th + longitude_step)) * radius;

                ArrayList<Vector3f> pos = new ArrayList<>(Arrays.asList(new Vector3f[]{
                        new Vector3f(z1, y1, x1),
                        new Vector3f(z2, y2, x2),
                        new Vector3f(z3, y3, x3),
                        new Vector3f(z4, y4, x4)
                }));

                ArrayList<Integer> triangles = new ArrayList<>(Arrays.asList(new Integer[]{
                        2, 0, 1,
                        3, 0, 2
                }));

                Vector3f n1 = new Vector3f(-x1, -y1, -z1).normalize();

                Vector3f n2 = new Vector3f(-x2, -y2, -z2).normalize();
                Vector3f n3 = new Vector3f(-x3, -y3, -z3).normalize();
                Vector3f n4 = new Vector3f(-x4, -y4, -z4).normalize();

                normals.add(n1);
                normals.add(n2);
                normals.add(n3);
                normals.add(n4);


                ArrayList<Vector2f> uv = new ArrayList<>(Arrays.asList(new Vector2f[]{
                        new Vector2f((float)(Math.atan2(n1.x, n1.z) / 2*pi) + 0.5f, n1.y * 0.5f + 0.5f),
                        new Vector2f((float)(Math.atan2(n2.x, n1.z) / 2*pi) + 0.5f, n2.y * 0.5f + 0.5f),
                        new Vector2f((float)(Math.atan2(n3.x, n1.z) / 2*pi) + 0.5f, n3.y * 0.5f + 0.5f),
                        new Vector2f((float)(Math.atan2(n4.x, n1.z) / 2*pi) + 0.5f, n4.y * 0.5f + 0.5f)
                }));

//                ArrayList<Vector3f> colors = new ArrayList<>(Arrays.asList(new Vector3f[]{
//                        new Vector3f(0.01f,0.1f, 1 - color/5),
//                        new Vector3f(0.01f,0.1f, 1 - color/5),
//                        new Vector3f(0.01f,0.1f, 1 - color/5),
//                        new Vector3f(0.01f,0.1f, 1 - color/5)
//                }));

                ArrayList<Vector3f> colors = new ArrayList<>(Arrays.asList(new Vector3f[]{
                        new Vector3f(1f, 1f, 1),
                        new Vector3f(1f, 1f, 1),
                        new Vector3f(1f, 1f, 1),
                        new Vector3f(1f, 1f, 1)
                }));

                meshData.append(pos, triangles, uv, colors);

            }
        }

        return new MeshBuilder()
                .addVector3fAttribute("aPosition", meshData.vertices)
                .addVector3fAttribute("aNormal", normals)
                .addVector2fAttribute("aTexCoord", meshData.uv)
                .setIndexBuffer(meshData.triangles)
                .loadShader("/shaders/basic")
                .create();

    }
}

