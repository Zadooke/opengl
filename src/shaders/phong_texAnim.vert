#version 330

uniform mat4 uProjection;
uniform mat4 uView;
uniform mat4 uWorld;
uniform vec3 uCameraPosition;

uniform float uTime;

in vec3 aPosition;
in vec3 aNormal;
in vec3 aColor;

in vec2 aTexCoord;

out vec4 vColor;
out vec3 vNormal;
out vec3 vViewPath;

out float vertDist;

out vec2 vTexCoord;

void main() {
    vec3 aPos = aPosition;

    aPos.y += (sin((aPos.x + uTime * .1 - 1) *  .1) + cos((aPos.z + uTime *.1 + 4) * .2))    * .2f;
    aPos.y += (sin((aPos.x + uTime *5 + 5)   *   1) + cos((aPos.z + uTime * 5 + 2) * .03f))  * .1f;
    aPos.y += (sin((aPos.x + uTime *1 - 4)   *  .2) + cos((aPos.z + uTime * 1 + 7) * 2))     * .2f;
    aPos.y += (sin((aPos.x + uTime *.5- 4)   * .04) + cos((aPos.z + uTime *.5 + 7) * 0.04))  * .4f;

    vColor = vec4(aColor, 1.0f);
    vNormal = (uWorld * vec4(aNormal, 0.0)).xyz;

    vec4 worldPos = uWorld * vec4(aPos, 1.0f);
    gl_Position =  uProjection * uView * worldPos;

    vNormal = (uWorld * vec4(aNormal, 0.0)).xyz;
    vViewPath = uCameraPosition - worldPos.xyz;

    vertDist = distance(uCameraPosition, worldPos.xyz);

    vTexCoord = aTexCoord;
}